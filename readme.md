# xBei.Redis.Extension

基于StackExchange.Redis的Redis操作类库

[![star](https://gitee.com/xbei/xBei.Redis.Extension/badge/star.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/stargazers)
[![fork](https://gitee.com/xbei/xBei.Redis.Extension/badge/fork.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/members)
[![xBei.Redis.Extension downloads](https://img.shields.io/nuget/dt/xBei.Redis.Extension)](https://www.nuget.org/packages/xBei.Redis.Extension)
[![xBei.Redis.Extension nuget](https://img.shields.io/nuget/v/xBei.Redis.Extension)](https://www.nuget.org/packages/xBei.Redis.Extension)

[文档](Src/readme.md)

## 扩展

* [xBei.Redis.Extensions.MsgPack](Extensions/xBei.Redis.Extensions.MsgPack/readme.md) - 使用MsgPack序列化数据
