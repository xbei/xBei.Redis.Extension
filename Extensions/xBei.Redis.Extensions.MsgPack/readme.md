﻿# xBei.Redis.Extensions.MsgPack

在[Redis](https://redis.io)中读写[MessagePack](https://msgpack.org/)数据的扩展方法。基于[MessagePack-CSharp](https://www.nuget.org/packages/messagepack)和[xBei.Redis.Extension](https://www.nuget.org/packages/xBei.Redis.Extension)。

[![star](https://gitee.com/xbei/xBei.Redis.Extension/badge/star.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/stargazers)
[![fork](https://gitee.com/xbei/xBei.Redis.Extension/badge/fork.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/members)
[![xBei.Redis.Extension.MsgPack downloads](https://img.shields.io/nuget/dt/xBei.Redis.Extension.MsgPack)](https://www.nuget.org/packages/xBei.Redis.Extension.MsgPack)
[![xBei.Redis.Extension.MsgPack nuget](https://img.shields.io/nuget/v/xBei.Redis.Extension.MsgPack)](https://www.nuget.org/packages/xBei.Redis.Extension.MsgPack)

## 安装

```bash
dotnet add package xBei.Redis.Extension.MsgPack
```