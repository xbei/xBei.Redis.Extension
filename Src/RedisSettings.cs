﻿namespace xBei.Redis.Extension {
    /// <summary>
    /// Redis配置
    /// </summary>
    public class RedisSettings : IRedisSettings {
        /// <summary>
        /// 
        /// </summary>
        public string RedisConnectionHost { get; set; } = default!;
        /// <summary>
        /// 
        /// </summary>
        public int RedisConnectionPort { get; set; } = 6379;
        /// <summary>
        /// 
        /// </summary>
        public string RedisConnectionPassword { get; set; } = default!;
        /// <summary>
        /// 
        /// </summary>
        public int RedisDb { get; set; }
    }
}
