# xBei.Redis.Extension

基于StackExchange.Redis的Redis操作类库

[![star](https://gitee.com/xbei/xBei.Redis.Extension/badge/star.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/stargazers)
[![fork](https://gitee.com/xbei/xBei.Redis.Extension/badge/fork.svg?theme=dark)](https://gitee.com/xbei/xBei.Redis.Extension/members)
[![xBei.Redis.Extension downloads](https://img.shields.io/nuget/dt/xBei.Redis.Extension)](https://www.nuget.org/packages/xBei.Redis.Extension)
[![xBei.Redis.Extension nuget](https://img.shields.io/nuget/v/xBei.Redis.Extension)](https://www.nuget.org/packages/xBei.Redis.Extension)

## 安装

```bash
dotnet add package xBei.Redis.Extension
```

## 更新历史

### 1.1.11

* 支持 net8.0


### 1.1.10

* 支持读写字节数组

### 1.1.9

* 简单实现订阅功能

### 1.1.8

* 移除 net5 支持
* 使用`RedisClientJsonConfig`配置JSON的系列化参数

### 1.1.7

* 移除 netcoreapp2.1 支持
* 使用`System.Text.Json`替换`Newtonsoft.Json`

## 用法

`redisClient.Database`可以直接使用`StackExchange.Redis.IDatabase`的所有方法。

`redisClient.SetIndex(0)`可以切换数据库。

### 配置JSON

``` c#
RedisClient.JsonConfig(options => {
    options.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
    options.WriteIndented = false,
    return options;
});

RedisClient.JsonConfig(options => {
    return new System.Text.Json.JsonSerializerOptions {
        Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping,
        WriteIndented = false,
    };
});
```
