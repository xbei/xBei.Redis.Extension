﻿using System;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace xBei.Redis.Extension {
    partial class RedisClient {
        /// <summary>
        /// 执行命令。
        /// 参考：<see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public RedisResult[] Execute(string command, params object[] args)
                    => ExecuteAsync(command, args).Result;
        /// <summary>
        /// 执行命令。
        /// 参考：<see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<RedisResult[]> ExecuteAsync(string command, params object[] args) {
            var r = await (await GetDatabaseAsync()).ExecuteAsync(command, args);
            return r.IsNull ? Array.Empty<RedisResult>() : (RedisResult[]?)r ?? Array.Empty<RedisResult>();
        }
        /// <summary>
        /// <see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public CursorResult ExecuteCursor(string command, params object[] args)
            => ExecuteCursorAsync(command, args).Result;
        /// <summary>
        /// <see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<CursorResult> ExecuteCursorAsync(string command, params object[] args) {
            var r = await ExecuteAsync(command, args);
            return new CursorResult {
                Cursor = r.Length > 1 ? (long)r[0] : 0,
                List = r.Length > 1
                    ? (RedisResult[]?)r[1] ?? Array.Empty<RedisResult>()
                    : Array.Empty<RedisResult>()
            };
        }
        /// <summary>
        /// 执行命令并返回<see cref="long"/>类型的值。
        /// 参考：<see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public long ExecuteLong(string command, params object[] args)
            => ExecuteLongAsync(command, args).Result;

        /// <summary>
        /// 执行命令并返回<see cref="long"/>类型的值。
        /// 参考：<see cref="IDatabase.Execute(string, object[])"/>
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public async Task<long> ExecuteLongAsync(string command, params object[] args) {
            return (long)await (await GetDatabaseAsync()).ExecuteAsync(command, args);
        }
        /// <summary>
        /// config get &lt;config&gt;
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public async Task<RedisResult[]> GetConfigAsync(string config) {
            var r = await (await GetDatabaseAsync()).ExecuteAsync("config", "get", config);
            return (RedisResult[]?)r ?? Array.Empty<RedisResult>();
        }
        /// <summary>
        /// config get &lt;config&gt;
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public async Task<int> GetIntConfigAsync(string config) {
            var r = await GetConfigAsync(config);
            return (int)r[1];
        }
        /// <summary>
        /// config get &lt;config&gt;
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public async Task<long> GetLongConfigAsync(string config) {
            var r = await GetConfigAsync(config);
            return (long)r[1];
        }
        #region SubClass
        /// <summary>
        /// 
        /// </summary>
        public class CursorResult {
            /// <summary>
            /// 游标
            /// </summary>
            public long Cursor { get; set; }
            /// <summary>
            /// 数据
            /// </summary>
            public StackExchange.Redis.RedisResult[] List { get; set; } = default!;
        }
        #endregion
    }
}
