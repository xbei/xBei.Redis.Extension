﻿namespace xBei.Redis.Extension {
    /// <summary>
    /// 
    /// </summary>
    public interface IRedisSettings {
        /// <summary>
        /// 服务器
        /// </summary>
        string RedisConnectionHost { get; }
        /// <summary>
        /// 端口
        /// </summary>
        int RedisConnectionPort { get; }
        /// <summary>
        /// 密码
        /// </summary>
        string RedisConnectionPassword { get; }
        /// <summary>
        /// 
        /// </summary>
        int RedisDb { get; }
    }
}
