﻿using MessagePack;
using xBei.Redis.Extension;
using xBei.Redis.Extensions.MsgPack;

namespace RedisClientDemo {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello, World!");
            var redis = new RedisClient("192.168.3.14", 6379, string.Empty, 1);
            TestBinAsync(redis).Wait();
        }
        static async Task TestBinAsync(RedisClient redis) {
            var user = new User { Name = "xBei", Age = 18 };
            await redis.SetMsgPackObjAsync("user", user);
            var buffer = await redis.GetBytesAsync("user");
            if (buffer != null) {
                user = await redis.GetMsgPackObjAsync<User>("user") ?? new User();
                Console.WriteLine(user.Name);
            }

            var x = await redis.DequeueMsgPackObjAsync<User>("abc");
            Console.WriteLine(x == null);
        }

    }
    [MessagePackObject]
    public class User {
        [Key(0)]
        public string Name { get; set; } = default!;
        [Key(1)]
        public int Age { get; set; }
    }
}